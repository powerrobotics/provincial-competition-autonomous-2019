void court06(){

  // Blue 1
  backwardBy(10, MAX_SPEED);
  forwardBy(9, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.25, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(90, MAX_SPEED);
  forwardBy(4, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(68, MAX_SPEED);
  forwardBy(12, MAX_SPEED);
  drop();
   
  // Blue 2 (Closest to Blue Square)
  backwardBy(4, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(15, MAX_SPEED);
  rotate(-90, MAX_SPEED);
  forwardBy(6, MAX_SPEED);
  drop();
  forwardBy(12, MAX_SPEED);

  // Red 1 (Red in middle)
  backwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(10, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(20, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(25, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(45, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(40, MAX_SPEED);
  drop();

  // Red 2 (Closest to Red Square)
  backwardBy(22, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(15, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(15, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(6, MAX_SPEED);
  drop();
  forwardBy(13, MAX_SPEED);
  
  // Yellow 1 (Yellow in middle)
  backwardBy(6, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(10, MAX_SPEED);
  backwardBy(18, TURN_SPEED);
  forwardBy(18, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(25, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(90, TURN_SPEED);
  pickup();
  backwardBy(27, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(30, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(64, MAX_SPEED);
  drop();

  // Yellow 2 (Top by Start)
  backwardBy(60, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(6, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(90, TURN_SPEED);
  pickup();
  backwardBy(6, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(18, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(58, MAX_SPEED);
  drop();
  forwardBy(13, MAX_SPEED);
  backwardBy(10, MAX_SPEED);

  return;
}
