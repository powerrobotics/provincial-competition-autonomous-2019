/* Michael Power / St. Joseph Autonomous 2019
 
  This file contains the basic actions the robot can 
  preform. These actions are to be used in other tabs 
  to make the robot do interesting things.
*/

//#define CLAW_SERVO_RIGHT 1
//#define CLAW_SERVO_LEFT 2
//#define DROP_GUARD 3

#define SENSOR_DELAY 20 // The ammount of mills to wait in between sensor checks

void rotate(double deg, int spd) {
  p.resetEncoders(); // reset encoders to clear the previous rotations

  // Using the circumference of the base, calculate how far the robot needs to spin to turn deg

  double rotateDistance = (deg / 360.0) * WHEEL_BASE_C;

  // Convert the distance needed to turn to degrees the wheels must turn
  double degreesToTurn = rotateDistance / WHEEL_C * 360;

  // Add the degrees to turn to the wheel
  double rightDeg = degreesToTurn * 4.0;
  double leftDeg = -degreesToTurn * 4.0;

  // Tell the robot to make the adjustments at a spd
  //p.setMotorDegrees(spd, rightDeg, spd, leftDeg);
  p.setMotorTargets(spd, rightDeg, spd, leftDeg);

  while(p.readMotorBusy(RIGHT_MOTOR) || p.readMotorBusy(LEFT_MOTOR)) {
    delay(SENSOR_DELAY);
  }
  //delay(100);
}

// Moves the robot forward at a spd for a time in mills
void forward(int mills, int spd) {
  p.setMotorSpeeds(MAX_SPEED, MAX_SPEED);
  delay(mills);
  p.setMotorSpeeds(0, 0); // turns the moters off when done
}

// Moves forward by inches at a spd
void forwardBy(double inches, int spd) {
  double deg = inches / WHEEL_C * 360.0;
  p.resetEncoders(); // reset encoders to clear the previous rotations
  p.setMotorDegrees(spd, deg, spd, deg);

  while(p.readMotorBusy(RIGHT_MOTOR) || p.readMotorBusy(LEFT_MOTOR)) {
    delay(SENSOR_DELAY);
  }
  //delay(100);  
}

// Moves Lift up by inches
void upBy(double inches) {
  double deg = inches / COG_C * 360.0;
  e.resetEncoders(1); // reset encoders to clear the previous rotations
  e.setMotorDegree(1, ELEVATOR_MOTOR, LIFT_SPEED, deg);

    while(e.readMotorBusy(1, ELEVATOR_MOTOR)) {
    delay(SENSOR_DELAY);
  }
}

// Moves Lift down by inches
void downBy(double inches) {
  double deg = -inches / COG_C * 360.0;
  e.resetEncoders(1); // reset encoders to clear the previous rotations
  e.setMotorDegree(1, ELEVATOR_MOTOR, LIFT_SPEED, deg);

    while(e.readMotorBusy(1, ELEVATOR_MOTOR)) {
    delay(SENSOR_DELAY);
  }
}

// Moves backward by inches at a spd
void backwardBy(double inches, int spd) {
  double deg = -inches / WHEEL_C * 360.0;
  p.resetEncoders(); // reset encoders to clear the previous rotations
  p.setMotorDegrees(spd, deg, spd, deg);

  while(p.readMotorBusy(RIGHT_MOTOR) || p.readMotorBusy(LEFT_MOTOR)) {
    delay(SENSOR_DELAY);
  }
  //delay(100);  
}

void stopMotors() {
    p.setMotorSpeeds(0, 0);
}

// Accelerates over mills from 0 dps to toSpeed
void accelerateFor(int mills, int toSpeed) {
  double stepCount = 10; // How many steps in the acceleration
  double timeStep = mills / stepCount; // how long does each step run for
  double speedStep = toSpeed / stepCount; // what is the difference in speed per step

  double totalSpeed = 0; // a counter for the current speed. Increases as the speed increases.

  for (int i = 0; i < stepCount; i++) { // For each step
    totalSpeed += speedStep; // Add the difference in speed to the total
    p.setMotorSpeeds(totalSpeed, totalSpeed); // Update the moter speed
    delay(timeStep); // Wait for the duration of this step
  }
}

// Decelerates over mills from fromSpeed to 0 dps
void decelerateFor(int mills, int fromSpeed) {
  double stepCount = 10; // How many steps in the decceleration
  double timeStep = mills / stepCount; // how long does each step run for
  double speedStep = fromSpeed / stepCount;

  double totalSpeed = fromSpeed;
  for (int i = 0; i < stepCount; i++) {
    totalSpeed -= speedStep;
    p.setMotorSpeeds(totalSpeed, totalSpeed);
    delay(timeStep);
  }
  p.setMotorSpeeds(0, 0); // Just ensure we always end at 0 dps
}

void setClaw(int deg) {
  p.setServoPosition(CLAW_SERVO_RIGHT, deg);
  p.setServoPosition(CLAW_SERVO_LEFT, 180 - deg);
}

void forwardUntilLine(int spd) {
  p.setMotorSpeeds(spd, spd);
  while(p.readLineSensor(IR_SENSOR) == 0) {}
  p.setMotorSpeeds(0, 0);
}

void backwardUntilLine(int spd) {
  p.setMotorSpeeds(-spd, -spd);
  while(p.readLineSensor(IR_SENSOR) == 0) {}
  p.setMotorSpeeds(0, 0);
}

void rotateLeftUntilLine(int spd) {
  p.setMotorSpeeds(-spd, spd);
  while(p.readLineSensor(IR_SENSOR) == 0) {}
  p.setMotorSpeeds(0, 0);
}

void rotateRightUntilLine(int spd) {
  p.setMotorSpeeds(spd, -spd);
  while(p.readLineSensor(IR_SENSOR) == 0) {}
  p.setMotorSpeeds(0, 0);
}

void forwardUntilDist(double dist_cm, int spd) {
  int current_spd = spd;
//  for (int i = 0; i < 10; i++) p.readSonicSensorCM(SONIC_SENSOR);
  long dist = 100000;
  p.setMotorSpeeds(spd, spd);
  while(dist > dist_cm) {
    dist = p.readSonicSensorCM(SONIC_SENSOR);
    Serial.println(dist);
    delay(2);
  }
  p.setMotorSpeeds(0, 0);
}

void centerOnForward(int spd) {
  int right_spd = spd;
  int left_spd = spd;
  while(right_spd > 0 || left_spd > 0) {
    float right_in = analogRead(SENSOR_RIGHT);
    float left_in = analogRead(SENSOR_LEFT);
    Serial.print(right_in);
    Serial.print("-");
    Serial.print(right_spd);
    Serial.print("   ");
    Serial.print(left_in);
    Serial.print("-");
    Serial.println(left_spd);
    if (analogRead(SENSOR_RIGHT) < COLOR_MIN) right_spd = -spd / 2.0;
    if (analogRead(SENSOR_LEFT) < COLOR_MIN) left_spd = -spd / 2.0;
    p.setMotorSpeeds(right_spd, left_spd);
//    delay(1);
//    millis();
  }
}
