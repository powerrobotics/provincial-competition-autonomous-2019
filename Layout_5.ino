void court05() {
    
  // RED 1
  backwardBy(20, TURN_SPEED);
  forwardBy(40, MAX_SPEED);
  rotate(95, TURN_SPEED);
  forwardBy(30, MAX_SPEED);
  pickup();
  backwardBy(5, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(12, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(58, MAX_SPEED);
  drop();

  // RED 2
  backwardBy(4, MAX_SPEED);
  rotate(180, TURN_SPEED);
  pickup();
  backwardBy(4, MAX_SPEED);
  rotate(180, TURN_SPEED);
  drop();
  forwardBy(15, MAX_SPEED);

  // BLUE 1
  backwardBy(10, MAX_SPEED);
  rotate(95, TURN_SPEED);
  forwardBy(23, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(20, MAX_SPEED);
  forwardBy(9, MAX_SPEED);
  rotate(92, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(45, TURN_SPEED);
  pickup();
  backwardBy(5, MAX_SPEED);
  rotate(45, TURN_SPEED);
  backwardBy(43, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(25, MAX_SPEED);
  drop();

  // BLUE 2
  backwardBy(26, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  rotate(45, TURN_SPEED);
  pickup();
  backwardBy(5, MAX_SPEED);
  rotate(45, TURN_SPEED);
  backwardBy(13, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(5, MAX_SPEED);
  backwardBy(13, MAX_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(3, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  drop();
  forwardBy(14, MAX_SPEED);

  // YELLOW 1
  backwardBy(15, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(10, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.5, MAX_SPEED);
  rotate(45, TURN_SPEED);
  pickup();
  backwardBy(18, MAX_SPEED);
  rotate(45, TURN_SPEED);
  backwardBy(30, MAX_SPEED);
  drop();

  // YELLOW 2
  backwardBy(35, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.5, MAX_SPEED);
  rotate(-45, TURN_SPEED);
  pickup();
  backwardBy(20, MAX_SPEED);
  rotate(135, TURN_SPEED);
  forwardBy(6, MAX_SPEED);
  drop();
  forwardBy(8, MAX_SPEED);
  backwardBy(10, MAX_SPEED);

  return;  
}
