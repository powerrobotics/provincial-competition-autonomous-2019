// Sorry Andre, we couldn't get your code to work so we redid it. We just changed the name of your function, its below still.
// By the way, dropping the first block in the middle of the square, and then pushing the second block onto the square works fantastically.
// - Carlo

void court04() {

  // FACE ROBOT TOWARDS WALL WITH BLOCKS

  // Blue 1
  forwardBy(23, MAX_SPEED); 
  forwardUntilDist(35, MAX_SPEED); // We use the ultrasonic to measure a distance of ~14 inches (35 cm)
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.25, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(90, MAX_SPEED);
  forwardBy(6, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(35, MAX_SPEED);
  drop();

  // Blue 2
  backwardBy(2, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(12, MAX_SPEED);
  forwardBy(60, MAX_SPEED);
  forwardUntilDist(35, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.25, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(90, MAX_SPEED);
  forwardBy(6, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  drop();
  forwardBy(18.5, MAX_SPEED);

  // Red 1
  backwardBy(4, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(12, MAX_SPEED);
  forwardBy(60, MAX_SPEED);
  forwardUntilDist(35, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.25, MAX_SPEED);
  rotate(90, TURN_SPEED);
  pickup();
  backwardBy(12, MAX_SPEED);
  rotate(90, MAX_SPEED);
  forwardBy(24, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(82, MAX_SPEED);
  forwardBy(6, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(28, MAX_SPEED);
  drop();

  // Red 2
  backwardBy(2, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(12, MAX_SPEED);
  forwardBy(60, MAX_SPEED); 
  forwardUntilDist(35, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.25, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(12, MAX_SPEED);
  rotate(-90, MAX_SPEED);
  backwardBy(16, MAX_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(42, MAX_SPEED);
  drop();
  forwardBy(9, MAX_SPEED); 

  // Yellow 1
  backwardBy(8, MAX_SPEED);
  rotate(180, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.25, MAX_SPEED);
  rotate(90, TURN_SPEED);
  pickup();
  backwardBy(8, MAX_SPEED);
  rotate(180, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(10, MAX_SPEED); 
  rotate(-90, TURN_SPEED);
  forwardBy(28, MAX_SPEED);
  drop();

  // Yellow 2
  backwardBy(15, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(48, MAX_SPEED);
  forwardBy(60, MAX_SPEED);
  forwardUntilDist(35, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1.25, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(12, MAX_SPEED);
  rotate(180, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(6, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(20, MAX_SPEED);
  drop();
  forwardBy(12, MAX_SPEED);
  backwardBy(6, MAX_SPEED);
}
