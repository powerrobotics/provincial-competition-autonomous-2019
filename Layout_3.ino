void court03() {
  
  // RED 1
  backwardBy(20, TURN_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(35, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2.5, MAX_SPEED);
  rotate(-50, TURN_SPEED);
  pickup();
  backwardBy(12, MAX_SPEED);
  rotate(-75, TURN_SPEED);
  forwardBy(43.25, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(25, TURN_SPEED);
  forwardBy(65, MAX_SPEED);
  drop();

  // RED 2
  backwardBy(8, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(80, TURN_SPEED);
  pickup();
  backwardBy(35, MAX_SPEED);
  forwardBy(7, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(4, MAX_SPEED);
  drop();
  forwardBy(9, MAX_SPEED);
  
  // BLUE 1
  backwardBy(4, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(52, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(-71, TURN_SPEED);
  pickup();
  delay(1);
  backwardBy(10, MAX_SPEED);
  rotate(68, TURN_SPEED);
  forwardBy(43, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(31, MAX_SPEED);
  forwardBy(50, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(10, MAX_SPEED);
  forwardBy(6, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(16.5, MAX_SPEED);
  drop();

    // BLUE 2
  backwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(-70, TURN_SPEED);
  pickup();
  backwardBy(8, MAX_SPEED);
  rotate(-110, TURN_SPEED);
  forwardBy(24, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  drop();
  forwardBy(5, TURN_SPEED);

    // YELLOW 1
  backwardBy(2, TURN_SPEED);
  rotate(9, TURN_SPEED);
  backwardBy(100, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(7, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(50, MAX_SPEED);
  forwardBy(2.25, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(14, MAX_SPEED);
  drop();

    // YELLOW 2
  backwardBy(10, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(60, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(38, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, MAX_SPEED);
  rotate(95, TURN_SPEED);
  pickup();
  backwardBy(3, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(65, MAX_SPEED);
  forwardBy(78, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(4, MAX_SPEED);
  drop();
  forwardBy(7, MAX_SPEED);
  backwardBy(5, TURN_SPEED);
  delay(1);

  
  return; 
}
