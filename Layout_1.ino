void court01() {

  // Blue 1
  forwardBy(23, MAX_SPEED);
  rotate(-45, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(90, TURN_SPEED);
  pickup();
  backwardBy(45, MAX_SPEED);
  forwardBy(5, MAX_SPEED); 
  rotate(-44, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  delay(500);
  rotate(44, TURN_SPEED);  
  forwardBy(25, MAX_SPEED);
  drop();

  // Red 1
  backwardBy(5, MAX_SPEED);
  rotate(180, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(90, TURN_SPEED);
  pickup();
  backwardBy(3, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(24, MAX_SPEED);
  drop();

  // Blue 2
  backwardBy(12, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(5, MAX_SPEED);
  rotate(-52, TURN_SPEED);
  forwardBy(33, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2.25, TURN_SPEED);
  rotate(-38, TURN_SPEED);
  pickup();
  backwardBy(6, MAX_SPEED);
  rotate(-89, TURN_SPEED);
  forwardBy(15, MAX_SPEED); 
  drop();
  forwardBy(11, MAX_SPEED);

  // Yellow 1
  backwardBy(11, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2.5, TURN_SPEED);
  rotate(-57, TURN_SPEED);
  forwardUntilDist(3, TURN_SPEED);
  delay(500);
  setClaw(CLAW_CLOSED);
  delay(500);
  backwardBy(20, MAX_SPEED);
  rotate(-63, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  rotate(35, TURN_SPEED);
  forwardBy(33.5, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(10, MAX_SPEED);
  drop();

  // Yellow 2
  backwardBy(5, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(9, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(3, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(12, MAX_SPEED);
  drop();
  forwardBy(14, MAX_SPEED);

  // Red 2
  backwardBy(40, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(10, TURN_SPEED);
  forwardBy(23.5, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, TURN_SPEED);
  rotate(-117, TURN_SPEED);
  forwardUntilDist(3, TURN_SPEED);
  delay(500);
  setClaw(CLAW_CLOSED);
  delay(500);
  backwardBy(10, MAX_SPEED);
  rotate(-60, TURN_SPEED);
  forwardBy(45, MAX_SPEED);
  rotate(93, TURN_SPEED);
  forwardBy(36, MAX_SPEED);
  drop();
  forwardBy(8, MAX_SPEED);
  backwardBy(11, MAX_SPEED);
  return;
  
}
