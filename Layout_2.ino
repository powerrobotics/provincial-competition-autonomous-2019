void court02() {

  // Red 1
  rotate(-8, TURN_SPEED);
  forwardBy(10, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(57, TURN_SPEED);
  pickup();
  backwardBy(25, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(36, MAX_SPEED);
  drop();

  // Yellow 1
  backwardBy(5, MAX_SPEED);
  rotate(117, TURN_SPEED); // was 115
  forwardUntilLine(MAX_SPEED);
  forwardBy(2, TURN_SPEED);
  rotate(-22, TURN_SPEED);
  pickup();
  backwardBy(25, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(35, MAX_SPEED);
  backwardBy(20, TURN_SPEED);
  forwardBy(5, MAX_SPEED); // WAS 7
  rotate(90, TURN_SPEED); 
  forwardBy(54, MAX_SPEED);
  drop();

  // Yellow 2
  
  backwardBy(6.25, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(4, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(20, MAX_SPEED); 
  rotate(90, TURN_SPEED);
  drop();
  forwardBy(9, MAX_SPEED);

  // Red 2
  backwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(24, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(34, MAX_SPEED);
  forwardBy(8, MAX_SPEED);
  rotate(90, TURN_SPEED);
  forwardBy(32, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  backwardBy(16, MAX_SPEED);
  forwardBy(55, MAX_SPEED);
  drop();
  forwardBy(10, MAX_SPEED);

  // Blue 1

  backwardBy(8, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(24, MAX_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  pickup();
  backwardBy(7, MAX_SPEED);
  rotate(90, TURN_SPEED);
  backwardBy(44, MAX_SPEED);
  forwardBy(6, MAX_SPEED);
  rotate(-90, TURN_SPEED);
  forwardBy(44, MAX_SPEED);
  drop();

  // Blue 2
  backwardBy(8, MAX_SPEED);
  rotate(91, TURN_SPEED);
  forwardUntilLine(MAX_SPEED);
  forwardBy(1, MAX_SPEED);
  rotate(90, TURN_SPEED);
  pickup();
  backwardBy(40, MAX_SPEED);
  forwardBy(9, MAX_SPEED);
  rotate(90, TURN_SPEED);
  drop();
  forwardBy(13, MAX_SPEED);
  backwardBy(6, MAX_SPEED);
  
  return;  

}
 
