/* Michael Power / St. Joseph Autonomous 2019

   This file is the program for the 2019 competition.

    Table of contents:

      - Comp-2019 - contains the main setup and loop methods and set variables
      - Base      - contains the basic operations used to complete the problem
      - Layout_1  - contains the operation for Court 1
      - Layout_2  - contains the operation for Court 2
      - Layout_3  - contains the operation for Court 3
      - Layout_4  - contains the operation for Court 4
      - Layout_5  - contains the operation for Court 5
      - Layout_6  - contains the operation for Court 6
*/

#include <PRIZM.h>

#define TURBO 1 // Turbo engaged?

#define SIDE_RIGHT -1 // a multiplyer to adjust angles for the right side
#define SIDE_LEFT 1 // a multiplyer to ajust angles for the left side

// Port numbers
#define RIGHT_MOTOR 2 // Right Motor Number
#define LEFT_MOTOR 1 // Left motor number
#define ELEVATOR_MOTOR 1
#define CLAW_SERVO_RIGHT 1
#define CLAW_SERVO_LEFT 2
#define SENSOR_RIGHT 1
#define SENSOR_LEFT 2
#define SONIC_SENSOR 2
#define IR_SENSOR 3

// Speeds
#define MAX_SPEED 150 //  0 to 720 degrees per second (DPS)
#define TURN_SPEED 90 //  0 to 720 degrees per second (DPS)
#define TURBO_SPEED 700 // DEEEEEEEPPPPPPPPSSSSSSS
#define LONG_DISTANCE_TURBO_SPEED 400
#define CLAW_SERVO_SPEED 40 // In percentage where 100% is max speed
#define LIFT_SPEED 150 // Lift Speed, 0 to 720 degrees oer second (DPS)

// Servo positions
#define CLAW_OPEN 120 // The claw's open position
#define CLAW_NEUTRAL 170 // Before lifting
#define CLAW_CLOSED 180 // The claw's closed position
#define CLAW_RELEASE 160 // The claw's release position

// Robot dimensions
#define WHEEL_WIDTH 4.0 // the wheel is 4 inches tall (Diameter)
#define WHEEL_C (PI * WHEEL_WIDTH) // the wheel circumference in inches
#define WHEEL_BASE_WIDTH 10.869 // the distance between the wheels in inches
#define WHEEL_BASE_C (PI * WHEEL_BASE_WIDTH) // the circumference of the weel base in inches
#define COG_DIAMETER 0.745 // Pitch Diameter of the Lift Pinon (Gear)
#define COG_C (PI * COG_DIAMETER) // Circumference of the Lift Pinon's (Gear) Pitch Circle

// Court Dimentions
#define LINE_DETECTION_THRESHOLD 1.0 // The values line sensors must detect to determine a line
#define ROTATION_CORRECTION_DIST 3.75 // The distance that the bot needs to go forward after detecting THE FARTHEST edge of line to line claw up for 90% turn in inches
#define SIDE_TAPE_TO_SKID_DIST 55.00 // 55.5 // The distance from the closest pipe rack tape to the skid
#define WALL_PROX_DIST 9 // How close to the wall in inches to trigger "Parallel park"
#define WALL_FAR_DIST 6.5 // How far the bot can be from the wall to trigger "Parallel park" // TODO: Fill In
#define FOURTH_PIPE_RETURN_DIST 60.0 // How far back to return for 4th pipe to prep for transition // TODO: Fill In
#define FOURTH_PIPE_TURBO_TIME 5750 //4200 // How long to run turbo back on 4th pipe
#define PICKUP_TURBO_DURATION 2100 //2800 // Milliseconds to run turbo for (inclides acceleration and deceleration) // TODO: Fill In

// Color thesholds for line and square detection
#define COLOR_MIN 500 //430
#define COLOR_MAX 630
#define BLACK_MIN 250
#define BLACK_MAX 420

PRIZM p; // Prizm library instance
EXPANSION e; // Expansion

void setup() {
  Serial.begin(9600); // For sending debug messages to a connected computer
  p.PrizmBegin();
  e.controllerEnable(1);
  p.setMotorInvert(1, 1);
  
  setClaw(CLAW_OPEN);
  upBy(8);
  //  p.setServoSpeed(CLAW_SERVO_NO, CLAW_SERVO_SPEED);
  downBy(0.75);
  delay(500);
}

void loop() {
  //court01();
  court02();
  //court03();
  //court04();
  //court05();
  //court06();

  //test();
  exit(0);
}

void drop() {
  //  delay(600);
  //  p.setServoPosition(CLAW_SERVO_RIGHT, CLAW_RELEASE);
  downBy(5.55);
  setClaw(CLAW_NEUTRAL);
  setClaw(CLAW_RELEASE);
  upBy(5.55);
  setClaw(CLAW_OPEN);
}

void pickup() {
  forwardUntilDist(4, TURN_SPEED);
  delay(500);
  setClaw(CLAW_CLOSED);
  delay(500);
}
